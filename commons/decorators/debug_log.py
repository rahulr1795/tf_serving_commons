import functools

from commons.logger import Logger


def log_debug(function):
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur
    """

    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        Logger.debug("Executing {}".format(function))
        return function(*args, **kwargs)

    return wrapper
