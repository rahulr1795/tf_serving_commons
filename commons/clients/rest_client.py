import json

import requests

from commons.clients.base_client import BaseClient


class RestClient(BaseClient):
    def __init__(self):
        super().__init__()

    def predict(self, data, meta_data=None):
        data = {"signature_name": "serving_default", "instances": data.tolist()}
        headers = {"content-type": "application/json"}
        json_response = requests.post(self.url, json=data, headers=headers)
        predictions = json.loads(json_response.text)['predictions'][0]
        if self.response_caster is not None:
            predictions = self.response_caster.cast(predictions)
        return predictions
