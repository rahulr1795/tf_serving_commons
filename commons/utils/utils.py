import json
import math

import numpy as np
import os


def crop(numpy_image, coordinates):
    x1, y1, x2, y2 = coordinates
    left = math.floor(x1)
    top = math.floor(y1)
    right = math.floor(x2)
    bottom = math.floor(y2)
    cropped_numpy_image = numpy_image[top:bottom, left:right]
    return cropped_numpy_image


def make_directory(path):
    if not os.path.exists(path):
        os.mkdir(path)

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """

    def default(self, obj):
        if isinstance(obj, (
                np.int_, np.intc, np.intp, np.int8, np.int16, np.int32, np.int64, np.uint8, np.uint16, np.uint32,
                np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
                              np.float64)):
            return float(obj)
        elif isinstance(obj, (np.ndarray,)):  #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
