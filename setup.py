from setuptools import setup

setup(
    name='commons',
    version='1.0',
    description='A useful module',
    author='Rahul R',
    author_email='rahul.r@attinadsoftware.com',
    packages=['commons', 'commons.data_processor', 'commons.exceptions', 'commons.schema',
              'commons.view', 'commons.utils', 'commons.decorators', 'commons.data_processor', 'commons.clients',
              'commons.services', 'commons.response_caster', 'commons.logger'],
    install_requires=['opencv-python', 'numpy', 'pillow', 'grpcio-tools', "quart", "tensorflow-serving-api==1.14.0",
                      "tensorflow==1.14.0",
                      "requests"]
)
