from commons.config import ModelSpec
from commons.config.base_config import BaseConfig


class TFServingConfig(BaseConfig):
    def __init__(self, config):
        super().__init__(config)
        self.model_spec: ModelSpec = config['model_spec']
        self.api = config['api']
        self.mode = config['mode']
