from commons.exceptions import BadRequestError, UnsupportedMediaTypeError

MIMETYPES = ['image/x-portable-graymap', 'image/x-portable-bitmap', 'image/x-portable-pixmap', 'image/cmu-raster',
             'image/x-cmu-raster', 'image/tiff', 'image/x-tiff', 'image/png', 'image/jpeg']


def validate_image(input):
    file = input.get('image')
    if file is None:
        raise BadRequestError("The field 'image' must not be empty")

    if file.mimetype not in MIMETYPES:
        raise UnsupportedMediaTypeError("This type of media is not supported")

    return file
