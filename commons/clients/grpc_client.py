from commons.clients.base_client import BaseClient
from commons.decorators.debug_log import log_debug

from tensorflow.contrib.util import make_tensor_proto
from tensorflow_serving.apis import predict_pb2


class GRPCClient(BaseClient):
    def __init__(self):
        super().__init__()

    @log_debug
    def predict(self, data):
        request = predict_pb2.PredictRequest()
        request.model_spec.name = self.model_spec.model
        request.model_spec.signature_name = self.model_spec.signature_name
        request.inputs[self.input_tensor_name].CopyFrom(make_tensor_proto(data))
        result = self.stub.Predict(request, 10.0)
        if self.response_caster is not None:
            result = self.response_caster.cast(result)
        return result
