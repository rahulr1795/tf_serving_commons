from abc import ABCMeta, abstractmethod


class AbstractResponseCaster:

    def __init__(self):
        pass

    @staticmethod
    @abstractmethod
    def cast(data):
        pass