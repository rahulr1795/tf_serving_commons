import json

from quart import jsonify

from commons.logger import Logger
from commons.utils import NumpyEncoder


class Response:
    def __init__(self, status_code, data=None, message=None):
        self.message = message
        self.status_code = status_code
        self.data = data

    def to_dict(self):
        response = dict(
            status=dict(
                code=self.status_code,
                message=self.message
            ),
        )

        if self.data is not None:
            response['data'] = json.loads(json.dumps(self.data, cls=NumpyEncoder))
        return jsonify(response)
        # return jsonify(dict(a=1))

    def make_response(self):
        return self.to_dict(), self.status_code


class UnsupportedMediaTypeError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


def handle_error(err):
    log_error(err)
    message, status = map_error(err)
    result = dict(code=status, message=message)
    response = dict(error=result)
    return jsonify(response), status


def log_error(err):
    error = err.__class__.__name__
    message = str(err)

    if error is "_Rendezvous":
        Logger.critical(message)
    else:
        Logger.error(message)


def map_error(err):
    message = str(err)
    error = err.__class__.__name__

    if error is "ServerError":
        status = 500

    elif error is "BadRequestError":
        status = 400

    elif error is "UnsupportedMediaTypeError":
        status = 415

    else:
        status = 500
        message = "Oops! Something went Wrong"

    return message, status
