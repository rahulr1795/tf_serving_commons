from commons.clients.grpc_client import GRPCClient
from commons.clients.rest_client import RestClient


class ClientFactory:

    @staticmethod
    def get_client(client_type):
        if client_type == 'gRPC':
            return GRPCClient()
        else:
            return RestClient()
