from commons.clients.client_factory import ClientFactory
from commons.config import TFServingConfig


class BasePredictionService:

    def __init__(self, config: TFServingConfig):
        self.input_tensor_name = None
        self.config = config
        self.response_caster = None

    def _get_client(self):
        client = ClientFactory.get_client(self.config.mode)
        client.initialize(self.config, self.input_tensor_name, self.response_caster)
        return client