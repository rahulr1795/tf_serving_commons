import functools
import time

from commons.logger import Logger


def log_info(function):
    """
    A decorator that wraps the passed in function and logs
    exceptions should one occur
    """

    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        Logger.info("Executing {}".format(function))
        return function(*args, **kwargs)

    return wrapper
