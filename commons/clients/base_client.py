from abc import abstractmethod

import grpc
from commons.config import TFServingConfig
from tensorflow.contrib.util import make_tensor_proto
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc

class BaseClient:

    def __init__(self):
        self.host = None
        self.grpc_port = None
        self.rest_port = None
        self.api = None
        self.url = None
        self.model_spec = None
        self.stub = None
        self.protocol = None
        self.is_secured = None
        self.channel = None
        self.response_caster = None
        self.input_tensor_name = None

    def initialize(self, config: TFServingConfig, input_tensor_name, response_caster=None):
        self.host = config.host
        self.api = config.api
        self.grpc_port = config.grpc_port
        self.rest_port = config.rest_port
        self.protocol = "https://" if self.is_secured else "http://"
        self.url = "{protocol}{host}:{port}/{api}".format(protocol=self.protocol, host=self.host, port=self.rest_port, api=self.api)
        self.model_spec = config.model_spec
        self.channel = grpc.insecure_channel('{host}:{port}'.format(host=self.host, port=self.grpc_port))
        self.stub = prediction_service_pb2_grpc.PredictionServiceStub(self.channel)
        self.response_caster = response_caster
        self.input_tensor_name = input_tensor_name

    @abstractmethod
    def predict(self, data):
        pass
