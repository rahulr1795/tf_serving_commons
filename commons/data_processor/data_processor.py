from abc import ABCMeta, abstractmethod


class AbstractDataProcessor(metaclass=ABCMeta):

    def __init__(self):
        pass

    @abstractmethod
    def pre_process(self, data, input_meta=None):
        pass

    @abstractmethod
    def post_process(self, data, input_meta=None):
        pass
