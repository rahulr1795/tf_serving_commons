from abc import ABCMeta


class ModelSpec:
    def __init__(self, model, signature_name):
        self.model = model
        self.signature_name = signature_name


#
class BaseConfig(metaclass=ABCMeta):

    def __init__(self, config):
        self.is_secure = config['is_secure']
        self.host = config['host']
        self.grpc_port = config['grpc_port']
        self.rest_port = config['rest_port']

# docker push with tag name
