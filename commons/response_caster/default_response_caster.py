from commons.response_caster.abstract_response_caster import AbstractResponseCaster


class DefaultResponseCaster(AbstractResponseCaster):

    def __init__(self):
        super().__init__()

    @staticmethod
    def cast(data):
        return data
